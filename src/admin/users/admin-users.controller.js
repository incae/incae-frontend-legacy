class AdminUsersController {
  /* @ngInject*/
  constructor(userServices, sessionServices, errorServices, ngNotify, $state, $stateParams, $log) {
    this.userServices = userServices;
    this.errorServices = errorServices;
    this.sessionServices = sessionServices;
    this.ngNotify = ngNotify;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.init();
  }

  init() {
    this.loadingUsers = true;
    this.currentUser = this.sessionServices.getCurrentUser();
    this.users = [];
    this.newUser = {};
    this.userServices.getAllUsersByRole('ADMIN').then(res => {
      this.users = res.data;
      this.loadingUsers = false;
    }).catch(res => this.errorServices.handleError(res));
  }

  openModal() {
    angular.element('#myModal').modal('toggle');
  }

  createAdmin() {
    if (!this.newUser.name || !this.newUser.login || !this.newUser.password) {
      this.ngNotify.set('Todos los campos son requeridos', 'error');
    } else {
      const role = {};
      role.id = 1;
      this.newUser.role = role;
      this.userServices.add(this.newUser).then(() => {
        this.ngNotify.set('Administrador creado con éxito', 'success');
        this.init();
      }).catch(res => {
        this.errorServices.handleError(res);
        this.ngNotify.set('Ha ocurrido un problema, intentalo de nuevo', 'error');
      });
    }
  }

  deleteAdmin(user) {
    this.userServices.delete(user.id).then(() => {
      this.ngNotify.set('Administrador eliminado con éxito', 'success');
      this.init();
    }).catch(res => {
      this.errorServices.handleError(res);
      this.ngNotify.set('Ha ocurrido un problema, intentalo de nuevo', 'error');
    });
  }
}

export default AdminUsersController;
