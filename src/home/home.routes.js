/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      template: require('./home.html'),
      controller: 'HomeController as vm'
    });
}

export default routes;
