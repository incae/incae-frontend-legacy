import savedGamesController from './saved-games.controller';
import routes from './saved-games.routes';
import './saved-games.scss';

export default
  angular
    .module('incae.saved-games', [])
    .config(routes)
    .controller('SavedGamesController', savedGamesController)
    .name;
