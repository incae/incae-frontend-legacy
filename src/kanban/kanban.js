import {KanbanController} from './kanban.controller';
import {KanbanServices} from './kanban.services';
import {TacticsServices} from '../tactics/tactics.services';
import {NavbarServices} from '../common/navbar/navbar.service';
import routes from './kanban.routes';
import './kanban.scss';

export default angular
  .module('incae.kanban', [])
  .config(routes)
  .controller('KanbanController', KanbanController)
  .service('KanbanServices', KanbanServices)
  .service('TacticsServices', TacticsServices)
  .service('NavbarServices', NavbarServices)
  .name;
