export class ChartServices {
  /* @ngInject */
  constructor(API_URL, $log, $http) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
  }

  changeState(collapse, id) {
    collapse[id] = !collapse[id];
  }

  setGraphOptions() {
    this.options = {
      chart: {
        type: "sunburstChart",
        height: 390,
        duration: 250,
        dispatch: {},
        sunburst: {
          dispatch: {},
          width: 350,
          height: 450,
          mode: "size",
          id: 2547,
          duration: 500,
          groupColorByParent: true,
          showLabels: false,
          labelThreshold: 0.02,
          margin: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
          }
        },
        tooltip: {
          valueFormatter: (d => {
            return d ? parseFloat(d).toFixed(2) : 0;
          }),
          duration: 0,
          gravity: "w",
          distance: 15,
          snapDistance: 0,
          classes: null,
          chartContainer: null,
          enabled: true,
          hideDelay: 200,
          headerEnabled: false,
          fixedTop: null,
          offset: {
            left: 0,
            to: 0
          },
          hidden: true,
          data: null,
          id: "nvtooltip-74967"
        },
        width: 450,
        mode: "size",
        groupColorByParent: true,
        showLabels: false,
        labelThreshold: 0.02,
        margin: {
          top: 30,
          right: 20,
          bottom: 20,
          left: 20
        },
        noData: null,
        defaultState: null
      },
      styles: {
        classes: {
          "with-3d-shadow": true,
          "with-transitions": true,
          "gallery": false
        },
        css: {}
      }
    };
    return this.options;
  }

  stageChartColor(stageName) {
    switch (stageName) {
      case 'Documentar':
        return '#85AFEF';
      case 'Identificar las causas':
        return '#0C4191';
      case 'Mapear':
        return '#3C506D';
      case 'Definir Vision':
        return '#A75F5B';
      case 'Comunicar':
        return '#C96C66';
      case 'Implementar y Medir':
        return '#A27ABD';
      default:
        return '#32875A';
    }
  }

  formatGraphData(rawData) {
    const result = [];
    rawData.forEach(stage => {
      const formattedStage = {
        name: stage.name,
        color: this.stageChartColor(stage.name),
        children: []
      };

      stage.decisions.forEach(decision => {
        const formattedDecision = {
          name: decision.name,
          size: decision.gainScore,
          color: this.stageChartColor(stage.name)
        };

        formattedStage.children.push(formattedDecision);
      });

      result.push(formattedStage);
    });

    return result;
  }
}
