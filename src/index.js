import common from './common/common';
import config from './index.config';

import {routesConfig, blockPrivateAccess, interceptor} from './index.routes';
import './index.scss';

import home from './home/home';
import savedGames from './saved-games/saved-games';
import results from './results/results';
import tactics from './tactics/tactics';
import kanban from './kanban/kanban';
import game from './game/game';
import people from './people/people';
import cases from './case/case';
import admin from './admin/admin';

export default
  angular
    .module('incae', [
      common,
      config,
      home,
      savedGames,
      results,
      tactics,
      kanban,
      game,
      people,
      cases,
      admin
    ])
    .config(interceptor)
    .config(routesConfig)
    .run(blockPrivateAccess);
