/* @ngInject */
function routes($stateProvider) {
  $stateProvider.state("kanban", {
    url: "/game/:id/kanban",
    template: require("./kanban.html"),
    controller: "KanbanController as vm"
  });
}

export default routes;
