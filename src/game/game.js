import gameController from './game.controller';
import gameCreateController from './create/game-create.controller';
import gameWelcomeController from './welcome/game-welcome.controller';
import gameExplanationController from './explanation/game-explanation.controller';
import {GameServices} from './game.services';
import {CaseServises} from '../case/case.services';
import routes from './game.routes';
import './game.scss';
import './create/game-create.scss';
import './welcome/game-welcome.scss';
import './explanation/game-explanation.scss';

import gameCreate from './create/game-create';

export default
  angular
    .module('incae.game', [
      gameCreate
    ])
    .config(routes)
    .controller('GameController', gameController)
    .controller('GameCreateController', gameCreateController)
    .controller('GameWelcomeController', gameWelcomeController)
    .controller('GameExplanationController', gameExplanationController)
    .service('gameServices', GameServices)
    .service('caseServises', CaseServises)
    .name;
