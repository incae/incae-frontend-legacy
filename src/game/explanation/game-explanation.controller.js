class GameExplanationController {
  /* @ngInject */
  constructor($state, $stateParams, $log) {
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.currentGameId = this.$stateParams.id;
  }
  goToGame() {
    this.$state.go('main', {id: this.currentGameId});
  }
}

export default GameExplanationController;
