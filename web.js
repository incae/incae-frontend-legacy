var gzippo = require('gzippo');
var express = require('express');
var app = express();
var copy = require('copy');

var environment = process.env.INCAE_ENV || 'production';

copy.one('conf/' + environment + '/env.js', 'dist', {flatten: true}, function(err, files) {
  if (err) throw err;
  // `files` is an array of the files that were copied 
});

app.use(gzippo.staticGzip('' + __dirname + '/dist'));
app.all('/*', function(req, res) {
    res.sendFile('/dist/index.html', { root: __dirname });
});
var port = process.env.PORT || 5000;
app.listen(port);
console.log('App listening in ' + environment + ' at port ' + port);