class NavbarController {
  /* @ngInject */
  constructor(gameServices, errorServices, $state, $stateParams, $log, $rootScope, sessionServices, navbarServices) {
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.sessionServices = sessionServices;
    this.navbarServices = navbarServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.$rootScope = $rootScope;
    this.game = {};
    this.$rootScope.$on('updateNavbarValues', this.getGameData.bind(this));
    this.init();
    this.showNavBar = true;
    this.leadershipChangeIcon = false;
    this.culturalChangeIcon = false;
    this.showNavBar = this.navbarServices.getCollapseState();
    this.leadershipChangeIcon = this.game.leadershipChangeScore > 50.0;
    this.culturalChangeIcon = this.game.culturalChangeScore > 50.0;
  }

  init() {
    this.sliderOptions = {
      floor: 1,
      ceil: 10,
      showTicks: true,
      readOnly: true,
      hideLimitLabels: true
    };
    this.getGameData();
  }

  updateDisplayValues() {
    this.game.daysBase10 = (10 / 360) * this.game.availableDays;
    this.game.budgetBase10 = this.game.budget / 100000;
    this.game.culturalChangeBase10 = this.game.culturalChangeScore / 10;
    this.game.leadershipChangeBase10 = this.game.leadershipChangeScore / 10;
  }

  getGameData() {
    const gameId = this.$stateParams.id;
    this.gameServices.getById(gameId).then(res => {
      this.game = res.data;
      this.updateDisplayValues();
    }).catch(res => this.errorServices.handleError(res));
  }

  exit() {
    this.sessionServices.destroy();
    this.$state.go('home');
  }

}

export default NavbarController;
