export class AuthorizationInjectorServices {
  /* @ngInject */
  constructor($log, sessionServices) {
    this.$log = $log;
    this.sessionServices = sessionServices;
    this.request = this.request.bind(this);
  }

  request(config) {
    if (this.sessionServices.isLoggedIn() && !config.headers.Authorization) {
      config.headers.Authorization = this.sessionServices.getAuthorization();
    }
    if (!config.file && !config.headers['Content-Type']) {
      config.headers['Content-Type'] = 'application/json';
    }
    return config;
  }
}
