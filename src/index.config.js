
import angular from 'angular';

const environment = {};

if (window) { // eslint-disable-line angular/window-service
  Object.assign(environment, window.__env); // eslint-disable-line angular/window-service
}

const env = angular.module("incae.config", [])
.constant("API_URL", environment.API_URL)
.constant("API_CLIENT_AUTH", environment.API_CLIENT_AUTH).name;

export default env;
