import BottomNavbarController from './bottom-navbar.controller';
import './bottom-navbar.scss';

const bottomNavbar = {
  template: require('./bottom-navbar.html'),
  controller: BottomNavbarController,
  controllerAs: 'vm'
};

export default
  angular
    .module('bottomNavbar', [])
    .component('bottomNavbar', bottomNavbar)
    .name;
