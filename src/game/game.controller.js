// import x from 'image-map';

class GameController {
  /* @ngInject*/
  constructor(gameServices, peopleServices, errorServices, $state, $stateParams, $timeout, $log) {
    // x.ImageMap('img[usemap]'); // eslint-disable-line babel/new-cap
    this.gameServices = gameServices;
    this.peopleServices = peopleServices;
    this.errorServices = errorServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$timeout = $timeout;
    this.$log = $log;
    this.game = {};
    this.departments = [];
    this.department = this.$stateParams.department || null;
    this.selectedDepartment = {};
    this.people = [];
    this.init();
  }

  init() {
    this.gameId = this.$stateParams.id;
    this.gameServices.getById(this.gameId).then(res => {
      this.game = res.data;
    }).catch(res => this.errorServices.handleError(res, true));

    this.peopleServices.getAllDepartmentsByGameId(this.gameId).then(res => {
      this.departments = res.data;
    }).catch(res => this.errorServices.handleError(res));

    if (this.department) {
      this.openDepartment(this.department);
    }
  }

  openDepartment(department) {
    this.selectedDepartment = department;
    this.peopleServices.getAllByDepartmentId(department.id).then(res => {
      this.people = res.data;
      angular.element('#myModal').modal('toggle');
    }).catch(res => this.errorServices.handleError(res));
  }

  goToPerson(person) {
    angular.element('#myModal').modal('toggle');
    this.$timeout(() => {
      this.$state.go('game-people', {game: this.gameId, id: person.id, department: this.selectedDepartment});
    }, 500);
  }
}

export default GameController;
