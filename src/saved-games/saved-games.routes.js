/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('saved-games', {
      url: '/saved-games',
      template: require('./saved-games.html'),
      controller: 'SavedGamesController as vm'
    });
}

export default routes;
