import angular from "angular";

export class TacticsServices {
  /* @ngInject */
  constructor(API_URL, $log, $http) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
  }

  getAll() {
    return this.$http.get(`${this.API_URL}/tactics`);
  }
  getBoards() {
    return this.$http.get(`${this.API_URL}/board`);
  }

  getDecisionsByGame(id) {
    return this.$http.get(`${this.API_URL}/decisions?gameId=${id}`);
  }

  getBoardByGame(id) {
    return this.$http.get(`${this.API_URL}/board?gameId=${id}`);
  }

  addDecision(gameId, tacticId, positionX, selectedEmployees = []) {
    const decision = {
      game: {
        id: gameId
      },
      tactic: {
        id: tacticId
      },
      posX: positionX,
      // eslint-disable-next-line babel/object-shorthand
      selectedEmployees: selectedEmployees
    };
    return this.$http.post(
      `${this.API_URL}/decisions`,
      angular.toJson(decision)
    );
  }

  updateDecision(decisionId, positionX) {
    const decision = {
      id: decisionId,
      posX: positionX
    };
    return this.$http.put(
      `${this.API_URL}/decisions`,
      angular.toJson(decision)
    );
  }

  applyDecision(decisionId) {
    const decision = {
      id: decisionId,
      isApplied: true
    };

    return this.$http.put(
      `${this.API_URL}/decisions`,
      angular.toJson(decision)
    );
  }

  deleteNotAppliedDecisions(gameId) {
    return this.$http.delete(`${this.API_URL}/delete-not-applied-decisions/${gameId}`);
  }

  addBoard(gameId, array = []) {
    const boardLayout = {
      game: gameId,
      // eslint-disable-next-line babel/object-shorthand
      tactics: array
    };
    return this.$http.post(
      `${this.API_URL}/board`,
      angular.toJson(boardLayout)
    );
  }

  deleteDecision(decisionId) {
    return this.$http.delete(`${this.API_URL}/decisions/${decisionId}`);
  }

  updateBoard(array1 = [], array2 = []) {
    const columna = {
      source: array1,
      target: array2
    };
    return this.$http.put(`${this.API_URL}/col`, angular.toJson(columna));
  }
}
