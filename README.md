Run Local
==========

To install dependencies:
```
npm install
```

To start application using API in localhost run:
```
npm run serve
```

To start application using API in remote environment run:
```
npm run serve:[<env>]
```
- Valid values for <env>: development, staging, production


Build Distribution
===================

To create build package run:
```
npm run build
```

To preview the built package run:
```
npm run preview
```

- The built package uses the API in production environment. To use another remote environment run:
```
export INCAE_ENV=<env>
```
- Valid values for <env>: development, staging, production


Deployment
===========

* Login to Heroku
* Add the Heroku remote and rename it:

```
heroku git:remote -a incae-<env>
git remote rename heroku incae-<env>
```

* Checkout to a version branch and tag it accordingly.
* Remove the `dist/` folder from the ignored paths.
* Push your changes:

```
git push incae-<env> v1:master
```