import NavbarController from './navbar.controller';
import {NavbarServices} from './navbar.service';
import './navbar.scss';

const navbar = {
  template: require('./navbar.html'),
  controller: NavbarController,
  controllerAs: 'vm',
  service: NavbarServices
};

export default
  angular
    .module('nav-header', [])
    .component('navbar', navbar)
    .service('navbarServices', NavbarServices)
    .name;
