class BottomNavbarController {
  /* @ngInject */
  constructor(gameServices, errorServices, $state, $stateParams, ngNotify, $log) {
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.ngNotify = ngNotify;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.game = {};
    this.init();
  }

  init() {
    const gameId = this.$stateParams.id;
    this.selected = this.$state.current.name;
    this.gameServices.getById(gameId).then(res => {
      this.game = res.data;
    }).catch(res => this.errorServices.handleError(res));
  }

  goToPeople() {
    this.$state.go('main', {id: this.game.id});
  }

  goToTactics() {
    this.$state.go('tactics', {id: this.game.id});
  }
  goToKanban() {
    this.$state.go('kanban', {id: this.game.id});
  }

  goToWelcome() {
    this.$state.go('game-welcome', {id: this.game.id});
  }

  goToResults() {
    if (this.game.status === 'FINISHED_LOST' || this.game.status === 'FINISHED_WON') {
      this.$state.go('results', {id: this.game.id});
    } else {
      this.ngNotify.set('No puedes acceder a resultados hasta que termines el juego', 'error');
    }
  }

  goToCases() {
    this.$state.go('cases', {id: this.game.id});
  }
}

export default BottomNavbarController;
