export class ResultServices {
  /* @ngInject */
  constructor(API_URL, $log, $http) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
  }

  getActionList(gameId) {
    return this.$http.get(`${this.API_URL}/decisions-by-stage?gameId=${gameId}`);
  }
}
