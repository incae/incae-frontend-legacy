class PeopleController {
  /* @ngInject*/
  constructor(peopleServices, errorServices, $state, $stateParams, $log) {
    this.peopleServices = peopleServices;
    this.errorServices = errorServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.department = this.$stateParams.department || null;
    this.$log = $log;
    this.person = {};
    this.init();
  }

  init() {
    this.gameId = this.$stateParams.game;
    const personId = this.$stateParams.id;
    this.peopleServices.getPerson(personId).then(res => {
      this.person = res.data;
      this.tabs = [
        {title: 'BIOGRAFIA', content: this.person.employeeInfo.background},
        {title: 'DIAGNOSTICO DE LA SITUACION', content: this.person.employeeInfo.scenarioDiagnosis},
        {title: 'SOLUCION PROPUESTA', content: this.person.employeeInfo.proposedSolution}
      ];
      this.selected = this.tabs[0];
    }).catch(res => this.errorServices.handleError(res, true));
  }

  select(tab) {
    this.selected = tab;
  }

  goToGame() {
// eslint-disable-next-line babel/object-shorthand
    this.$state.go('main', {id: this.gameId, department: this.department});
  }
}

export default PeopleController;
