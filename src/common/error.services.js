export class ErrorServices {
  /* @ngInject */
  constructor(sessionServices, ngNotify, $log, $state) {
    this.sessionServices = sessionServices;
    this.ngNotify = ngNotify;
    this.$log = $log;
    this.$state = $state;
  }

  handleError(res, isRedirect, redirectTo) {
    if (res.status === 409) {
      this.ngNotify.set('Lo sentimos, este nombre de usuario ya existe. Por favor elige otro.', 'error');
    } else if (res.status === 423) {
      this.ngNotify.set('Lo sentimos, este nombre de usuario ya existe entre los jugadores. Por favor elige otro.', 'error');
    } else if (res.status === 404) {
      this.ngNotify.set('Lo sentimos, esta página no existe.', 'error');
      if (isRedirect && redirectTo) {
        this.$state.go(redirectTo);
      } else if (isRedirect) {
        this.$state.go('home');
      }
    } else if (res.status === 401) {
      this.sessionServices.destroy();
      this.ngNotify.set('Su sesión ha expirado', 'error');
      if (this.$state.current.private) {
        this.$state.go('login');
      } else {
        this.$state.go('home');
      }
    } else if (res.status === -1 || res.status === 503) {
      this.ngNotify.set('Lo sentimos, nuestro servidor no está disponible. Por favor intenta más tarde.', 'error');
    } else {
      this.ngNotify.set('Lo sentimos, algo hicimos mal. Por favor intenta más tarde.', 'error');
    }
  }

  handleErrorWithPreconditions(res, message) {
    if (res.status === 412) {
      this.ngNotify.set(message, {type: 'error', duration: 5000});
    } else {
      this.handleError(res);
    }
  }

  handlerErrorWithSessionCredentials(error) {
    if (error.status === 410) {
      this.ngNotify.set('La contraseña ha expirado', 'error');
    } else if (error.status === 406) {
      this.ngNotify.set('El nombre de usuario no existe', 'error');
    } else if (error.status === 400) {
      this.ngNotify.set('El nombre de usuario o contraseña son incorrectos', 'error');
    } else {
      this.handleError(error);
    }
  }
}
