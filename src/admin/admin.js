import adminController from './admin.controller';
import adminGameController from './game/admin-game.controller';
import loginController from './login/login.controller';
import adminUsersController from './users/admin-users.controller';
import consolidatedResultsController from './consolidated-results/consolidated-results.controller';
import {LoginServices} from './login/login.services';
import {UserServices} from './users/user.services';
import {ChartServices} from '../chart/chart.services';
import routes from './admin.routes';
import './admin.scss';
import './game/admin-game.scss';
import './login/login.scss';
import './users/admin-users.scss';
import './consolidated-results/consolidated-results.scss';

import adminNavbar from './navbar/admin-navbar';

export default
  angular
    .module('incae.admin', [
      adminNavbar
    ])
    .config(routes)
    .controller('AdminController', adminController)
    .controller('AdminGameController', adminGameController)
    .controller('LoginController', loginController)
    .controller('AdminUsersController', adminUsersController)
    .controller('ConsolidatedResultsController', consolidatedResultsController)
    .service('loginServices', LoginServices)
    .service('userServices', UserServices)
    .service('chartServices', ChartServices)
    .name;
