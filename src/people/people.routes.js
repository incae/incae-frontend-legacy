/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('game-people', {
      url: '/game/:game/people/:id',
      template: require('./people.html'),
      controller: 'PeopleController as vm',
      params: {department: null}
    });
}

export default routes;
