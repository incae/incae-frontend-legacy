class LoginController {
  /* @ngInject*/
  constructor(loginServices, errorServices, sessionServices, $state, ngNotify, $log, userServices) {
    this.loginServices = loginServices;
    this.errorServices = errorServices;
    this.sessionServices = sessionServices;
    this.userServices = userServices;
    this.$state = $state;
    this.ngNotify = ngNotify;
    this.$log = $log;
    this.user = {};
    this.roles = ['ADMIN'];
  }

  login() {
    if (!this.emptyLoginValues()) {
      this.userServices.userRoleValidation(this.user.username, this.roles).then(() => {
        this.loginServices.login(this.user.username, this.user.password).then(res => {
          this.sessionServices.setAuthData({username: this.user.username}, res.data);
          this.$state.go('admin');
        }).catch(error => {
          this.sessionServices.destroy();
          this.errorServices.handlerErrorWithSessionCredentials(error);
        });
      }).catch(error => {
        this.errorServices.handlerErrorWithSessionCredentials(error);
      });
    }
  }

  emptyLoginValues() {
    let pass = false;
    if (!this.user.username) {
      this.ngNotify.set('Debe ingresar un nombre de usuario', 'error');
      pass = true;
    } else if (!this.user.password) {
      this.ngNotify.set('Debe ingresar una contraseña', 'error');
      pass = true;
    }
    return pass;
  }

  goToGame() {
    this.$state.go('home');
  }
}

export default LoginController;
