class AdminGameController {
  /* @ngInject*/
  constructor(gameServices, tacticsServices, errorServices, $state, $stateParams, $log, resultServices, chartServices) {
    this.gameServices = gameServices;
    this.tacticsServices = tacticsServices;
    this.errorServices = errorServices;
    this.resultServices = resultServices;
    this.chartServices = chartServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.game = {};
    this.teamMembers = [];
    this.decisions = [];
    this.tactics = [];
    this.init();
  }

  init() {
    const gameId = this.$stateParams.id;

    this.gameServices.getById(gameId).then(res => {
      this.game = res.data;
    }).catch(res => this.errorServices.handleError(res, true, 'admin'));

    this.gameServices.getTeamMembers(gameId).then(res => {
      this.teamMembers = res.data;
    }).catch(res => this.errorServices.handleError(res));

    this.tacticsServices.getDecisionsByGame(gameId).then(decisionsResult => {
      this.decisions = this.sortDecisions(decisionsResult.data);
      this.tacticsServices.getAll().then(tacticsResult => {
        this.tactics = tacticsResult.data;
        this.disableAppliedTactics();
      }).catch(res => this.errorServices.handleError(res));
    }).catch(res => this.errorServices.handleError(res));

    this.resultServices.getActionList(gameId).then(res => {
      this.actionsList = res.data;
      this.graphData = [{
        name: "Puntaje",
        children: this.chartServices.formatGraphData(res.data)
      }];
      this.gameServices.getById(gameId).then(res => {
        this.game = res.data;
      }).catch(res => this.errorServices.handleError(res, true));
    }).catch(res => this.errorServices.handleError(res, true));
  }

  disableAppliedTactics() {
    this.tactics.forEach(tactic => {
      tactic.disabled = false;
      for (let i = 0; i < this.decisions.length; i++) {
        if (this.decisions[i].tactic.id === tactic.id) {
          tactic.disabled = true;
          break;
        }
      }
    });
  }

  goBack() {
    this.$state.go('admin');
  }

  deleteGame() {
    this.gameServices.delete(this.game.id).then(() => {
      this.$state.go('admin');
    }).catch(res => this.errorServices.handleError(res));
  }

  changeState(id) {
    this.chartServices.changeState(this.collapse, id);
  }

  sortDecisions(decisions) {
    return decisions.sort((decisionA, decisionB) => {
      const {tactic: {stage: stageA}} = decisionA;
      const {tactic: {stage: stageB}} = decisionB;
      return (stageA.id - stageB.id);
    });
  }
}

export default AdminGameController;
