class AdminNavbarController {
  /* @ngInject */
  constructor(sessionServices, errorServices, $state, $stateParams, $log) {
    this.sessionServices = sessionServices;
    this.errorServices = errorServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.init();
  }

  init() {
    this.current = this.$state.current.name;
  }

  goToAdminTeams() {
    this.$state.go('admin');
  }

  goToAdministrators() {
    this.$state.go('admin-users');
  }

  goToConsolidatedResults() {
    this.$state.go('consolidated-results');
  }

  logout() {
    this.sessionServices.destroy();
    this.$state.go('login');
  }
}

export default AdminNavbarController;
