class HomeController {
  /* @ngInject */
  constructor($log, $state, loginServices, sessionServices, gameServices, errorServices, ngNotify) {
    this.$log = $log;
    this.$state = $state;
    this.init();
    this.errorServices = errorServices;
    this.ngNotify = ngNotify;
    this.loginServices = loginServices;
    this.sessionServices = sessionServices;
    this.gameServices = gameServices;
    this.user = {};
  }

  init() {
  }

  login() {
    if (!this.emptyLoginValues()) {
      this.gameServices.getGameByUsername(this.user.username).then(resgame => {
        this.loginServices.login(this.user.username, this.user.password).then(res => {
          this.sessionServices.setAuthData({username: this.user.username}, res.data);
          this.$state.go('game-welcome', {id: resgame.data.id});
        }).catch(error => {
          this.sessionServices.destroy();
          this.errorServices.handlerErrorWithSessionCredentials(error);
        });
      }).catch(error => {
        this.errorServices.handlerErrorWithSessionCredentials(error);
      });
    }
  }

  emptyLoginValues() {
    let pass = false;
    if (!this.user.username) {
      this.ngNotify.set('Debe ingresar un nombre de usuario', 'error');
      pass = true;
    } else if (!this.user.password) {
      this.ngNotify.set('Debe ingresar una contraseña', 'error');
      pass = true;
    }
    return pass;
  }
}

export default HomeController;
