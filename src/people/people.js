import {PeopleServices} from './people.services';
import peopleController from './people.controller';

import routes from './people.routes';
import './people.scss';

export default
  angular
    .module('incae.people', [])
    .config(routes)
    .controller('PeopleController', peopleController)
    .service('peopleServices', PeopleServices)
    .name;
