/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('tactics', {
      url: '/game/:id/decision',
      template: require('./tactics.html'),
      controller: 'TacticsController as vm'
    });
}

export default routes;
