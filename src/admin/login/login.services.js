export class LoginServices {
/* @ngInject */
  constructor(API_URL, API_CLIENT_AUTH, $log, $http, $httpParamSerializer) {
    this.API_URL = API_URL;
    this.API_CLIENT_AUTH = API_CLIENT_AUTH;
    this.$log = $log;
    this.$http = $http;
    this.$httpParamSerializer = $httpParamSerializer;
  }

  login(username, password) {
    const request = {
      grant_type: "password", // eslint-disable-line camelcase
      username,
      password
    };

    return this.$http({
      url: `${this.API_URL}/oauth/token`,
      method: "POST",
      data: this.$httpParamSerializer(request),
      headers: {
        "Authorization": this.API_CLIENT_AUTH,
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8;"
      }
    });
  }
}
