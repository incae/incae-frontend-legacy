class TacticsController {
  /* @ngInject */
  constructor(tacticsServices, peopleServices, gameServices, errorServices, ngNotify, $filter, $state, $stateParams, $log, $rootScope) {
    this.tacticsServices = tacticsServices;
    this.peopleServices = peopleServices;
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.ngNotify = ngNotify;
    this.$filter = $filter;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.$rootScope = $rootScope;
    this.gameId = null;
    this.tactic = null;
    this.tactics = [];
    this.decisions = [];
    this.propertyName = 'name';
    this.reverse = false;
    this.collapse = [];
    this.people = [];
    this.loading = true;
    this.chartData = null;
    this.lastPos = -1;
    this.selectedArray = [];
    this.handleMouseOver = e => {
      this.rowSel = e;
    };
    this.init();
  }

  init() {
    this.gameId = this.$stateParams.id;
    google.charts.load('45', {packages: ['orgchart']}); // eslint-disable-line no-undef
    this.tacticsServices.getDecisionsByGame(this.gameId).then(decisionsResult => {
      this.decisions = decisionsResult.data;
      this.tacticsServices.getAll().then(tacticsResult => {
        this.tactics = tacticsResult.data;
        this.disableAppliedTactics();
        this.loading = false;
      }).catch(res => this.errorServices.handleError(res));
    }).catch(res => this.errorServices.handleError(res));
  }

  disableAppliedTactics() {
    this.tactics.forEach(tactic => {
      tactic.disabled = false;
      for (let i = 0; i < this.decisions.length; i++) {
        if (this.decisions[i].tactic.id === tactic.id) {
          tactic.disabled = true;
          break;
        }
      }
    });
  }

  checkGameFinished() {
    this.gameServices.getById(this.gameId).then(res => {
      const game = res.data;
      if (game.status === "FINISHED_LOST" || game.status === "FINISHED_WON") {
        this.$state.go('results', {id: this.gameId});
      } else {
        this.$rootScope.$emit('updateNavbarValues', {});
        this.init();
      }
    }).catch(res => this.errorServices.handleError(res));
  }

  applyTactic(tactic) {
    this.tacticsServices.addDecision(this.gameId, tactic.id).then(res => {
      if (res.data) {
        const feedbackOptimum = res.data.tactic.tacticScore.feedbackOptimum;
        const feedbackNonOptimum = res.data.tactic.tacticScore.feedbackNonOptimum;
        const longOptimum = feedbackOptimum.length > 50;
        const longNonOptimum = feedbackNonOptimum.length > 50;
        if (res.data.scoreLevel === "PERFECT" && res.data.tactic.tacticScore.perfect !== 0) {
          this.ngNotify.set(`Decisión aplicada oportunamente. ${feedbackOptimum}`, {type: 'success', sticky: longOptimum, position: "bottom", button: true});
        } else if (res.data.scoreLevel === "NEGATIVE") {
          this.ngNotify.set(`Decisión aplicada. ${feedbackNonOptimum}`, {type: 'error', sticky: longNonOptimum, position: "bottom", button: true});
        } else {
          this.ngNotify.set(`Decisión aplicada. ${feedbackNonOptimum}`, {type: 'info', sticky: longNonOptimum, position: "bottom", button: true});
        }
        this.checkGameFinished();
      }
    }).catch(res => this.errorServices.handleErrorWithPreconditions(res,
      'Su equipo no cuenta con el suficiente presupuesto o ya no cuenta con días disponibles para tomar esta decisión'));
  }

  changeCollapseState(id) {
    this.collapse[id] = !this.collapse[id];
  }

  getSelectedEmployees(people) {
    const selectedEmployees = [];
    people.forEach(person => {
      if (person.selected) {
        selectedEmployees.push({id: person.id});
      }
    });
    return selectedEmployees;
  }

  // this have to change when implementent logic of selected employees
  changeSelected(person) {
    person.selected = !person.selected;
  }

  sortBy(propertyName) {
    this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
    this.propertyName = propertyName;
  }

  drawVisualization(tactic) {
    // Draw a column chart
    this.tactic = tactic;
    this.cleanSelection();
    this.peopleServices.getAllInfo().then(res => {
      try {
        this.people = res.data;
        const data = [['Name', 'reportsTo', 'ToolTip']];
        this.peopleDataSetFormatter(data);
        this.chartData = {
          chartType: 'OrgChart',
          dataTable: data,
          options: {
            allowHtml: true,
            color: "#879294",
            selectionColor: "#34495E"
          }
        };

        const doc = document.getElementById('orgChart'); // eslint-disable-line angular/document-service
        const orgChartWrapper = new google.visualization.ChartWrapper(this.chartData); // eslint-disable-line no-undef
        orgChartWrapper.setDataTable(data);
        orgChartWrapper.draw(doc);
        const chart = orgChartWrapper.getChart();
        google.visualization.events.addListener(chart, 'onmouseover', this.handleMouseOver);  // eslint-disable-line no-undef
        google.visualization.events.addListener(orgChartWrapper, 'select', () => {  // eslint-disable-line no-undef
          const position = this.rowSel.row;
          if (this.isSelected(this.rowSel.row)) {
            this.deselectElement({row: this.rowSel.row, column: this.rowSel.column});
          } else {
            this.selectedArray.push({row: this.rowSel.row, column: this.rowSel.column});
          }
          this.changeSelected(this.people[position]);
          chart.setSelection(this.selectedArray);
          this.lastPos = position;
        });
        angular.element('#involvedEmployeesModal').modal('toggle');
      } catch (error) {
        this.errorServices.handleError(error);
      }
    }).catch(res => this.errorServices.handleError(res));
  }

  peopleDataSetFormatter(rows) {
    const president = this.people[0];
    let lastPerson = president;
    this.people.forEach(person => {
      const name = (person.jobTitle === 'Presidente') ? '' : lastPerson.name;
      rows.push([
        {v: person.name, f: this.personDivStyleFormatter(person)},
        (person.departmentInfo.name === lastPerson.departmentInfo.name) ? name : president.name,
        person.background
      ]);
      lastPerson = person;
    });
    return rows;
  }

  personDivStyleFormatter(person) {
    const departmentDiv = `<div class="modal-person-department">${person.departmentInfo.name}</div>`;
    const imageDiv = `<img style="width:75px; height:75px;" src="../img/employees/${person.profilePicture}">`;
    const nameDiv = `<div class="modal-person-name">${person.name}</div>`;
    const infoDiv = `<div class="modal-person-title">${person.jobTitle}</div>`;
    return `<div class="modal-org-chart-${person.departmentInfo.name.split(" ")[0]}"> ${imageDiv} ${nameDiv} ${infoDiv} ${departmentDiv} </div>`;
  }

  deselectElement(selected) {
    let flag = true;
    for (let i = 0; i < this.selectedArray.length && flag; i++) {
      if (selected.column === this.selectedArray[i].column && selected.row === this.selectedArray[i].row) {
        this.selectedArray.splice(i, 1);
        flag = false;
      }
    }
  }

  cleanSelection() {
    this.people.forEach(person => {
      person.selected = false;
    });
    this.selectedArray = [];
    this.lastPos = -1;
  }

  isSelected(row) {
    for (let i = 0; i < this.selectedArray.length; i++) {
      if (this.selectedArray[i].row === row) {
        return true;
      }
    }
    return false;
  }
}

export default TacticsController;
