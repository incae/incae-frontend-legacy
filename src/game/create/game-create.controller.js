class GameCreateController {
  /* @ngInject */
  constructor(gameServices, errorServices, ngNotify, $state, $log) {
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.ngNotify = ngNotify;
    this.$state = $state;
    this.$log = $log;
    this.teamMembers = [];
    this.teamMember = {};
    this.game = {};
    this.regex = /^[a-zA-Z_\s]*$/;
    this.isCreatingGame = false;
    this.newTeamMemberLoginName = "";
  }
  createGame() {
    if (!this.game.name) {
      this.ngNotify.set('El nombre de equipo es requerido', 'error');
    } else if (!this.regex.test(this.game.name)) {
      this.ngNotify.set('El nombre del equipo no debe contener caracteres especiales o números', 'error');
    } else if (this.teamMembers.length === 0) {
      this.ngNotify.set('Debe agregar al menos un jugador', 'error');
    } else if (!this.game.user.password || this.game.user.password.length < 8) {
      this.ngNotify.set('La contraseña debe tener un mínimo de 8 caracteres', 'error');
    } else {
      this.isCreatingGame = true;
      this.game.user.login = this.game.name;
      this.game.user.name = this.game.name;
      this.gameServices.addGame(this.game).then(res => {
        this.game = {};
        this.createTeamMembers(res.data);
        this.ngNotify.set('El equipo se ha creado con éxito', 'success');
      }).catch(res => {
        this.isCreatingGame = false;
        this.errorServices.handleError(res);
        this.ngNotify.set('Ha ocurrido un problema, intentalo de nuevo', 'error');
      });
    }
  }
  createTeamMembers(game) {
    this.gameServices.addTeamMembers(this.teamMembers, game.id).then(() => {
      this.game.name = "";
      this.teamMembers = [];
      this.isCreatingGame = false;
    }).catch(error => {
      this.isCreatingGame = false;
      this.usernamesConflict(error.data);
      this.errorServices.handleError(error);
    });
  }
  addTeamMember(teamMember) {
    if (!this.teamMember.name) {
      this.ngNotify.set('El nombre del jugador es requerido', 'error');
    } else if (this.regex.test(this.teamMember.name) === false) {
      this.ngNotify.set('El nombre del jugador no debe contener caracteres especiales o números', 'error');
    } else {
      this.teamMembers.push(teamMember);
      this.teamMember = {};
    }
  }
  removeTeamMember(teamMember) {
    this.teamMembers.splice(this.teamMembers.indexOf(teamMember), 1);
  }
}

export default GameCreateController;
