/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('results', {
      url: '/results/:id',
      template: require('./results.html'),
      controller: 'ResultsController as vm'
    });
}

export default routes;
