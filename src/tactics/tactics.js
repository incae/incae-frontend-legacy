import TacticsController from './tactics.controller';
import {TacticsServices} from './tactics.services';
import routes from './tactics.routes';
import './tactics.scss';

export default
  angular
    .module('incae.tactics', [])
    .config(routes)
    .controller('TacticsController', TacticsController)
    .service('tacticsServices', TacticsServices)
    .name;
