/* eslint-disable */
import { range, timeHours } from "d3";
import Sortable from "sortablejs";
import swal from "sweetalert2";

export class KanbanController {
  constructor(
    navbarServices,
    tacticsServices,
    peopleServices,
    gameServices,
    errorServices,
    ngNotify,
    $filter,
    $document,
    $state,
    $stateParams,
    $log,
    $rootScope,
    $scope
  ) {
    this.tacticsServices = tacticsServices;
    this.navbarServices = navbarServices;
    this.peopleServices = peopleServices;
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.ngNotify = ngNotify;
    this.$filter = $filter;
    this.$state = $state;
    this.$document = $document;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.gameId = null;
    this.tactic = null;
    this.tactics = [];
    this.decisions = [];
    this.tacticIds = [];
    this.propertyName = "name";
    this.reverse = false;
    this.collapse = [];
    this.people = [];
    this.loading = true;
    this.firstRender = true;
    this.chartData = null;
    this.lastPos = -1;
    this.allcols = [[], [], [], [], [], [], []];
    this.plannedDays = 0;
    this.plannedBudget = 0;
    this.appliedBudget = 0;
    this.appliedDays = 0;
    this.gameBudget = 1000000;
    this.gameDays = 360;
    this.game = null;
    this.handleMouseOver = (e) => {
      this.rowSel = e;
    };
    this.rawData = null;
    this.init();
  }

  async init() {
    await this.getGameInformation();
    this.assingGameID();
    await this.getDecisionByGameID();
    await this.getTactics();
    google.charts.load("45", { packages: ["orgchart"] }); // eslint-disable-line no-undef
    this.dragElements();
  }

  dragElements() {
    let elemArr = [];
    for (let i = 0; i < 7; i++) {
      elemArr.push(document.getElementById(`${i}`));
    }
    elemArr.forEach((elem) => {
      Sortable.create(elem, {
        group: "kanban",
        animation: 50,
        easing: "cubic-bezier(0, 0, 1, 1)",
        preventOnFilter: true,
        draggable: ".task",
        removeCloneOnHide: true,
        onEnd: (/**Event*/ evt) => {
          let tactic = evt.item; // dragged HTMLElement
          let from = parseInt(evt.from.id);
          const posX = parseInt(evt.to.id);
          if (from != posX) {
            this.addDecision(tactic, posX);
          }
        },
      });
    });
  }

  async getTactics() {
    await this.tacticsServices
      .getAll()
      .then((tacticsResult) => {
        this.tactics = tacticsResult.data;
        this.loading = false;
        this.getAssignItems();
      })
      .catch((res) => {
        this.errorServices.handleError(res);
      });
  }

  assingGameID() {
    this.gameId = parseInt(this.$stateParams.id);
  }

  async getDecisionByGameID() {
    await this.tacticsServices
      .getDecisionsByGame(this.gameId)
      .then((decisioncResult) => {
        this.decisions = decisioncResult.data;
        this.getAddedTacticIDs(decisioncResult.data);
        for (let i = 0; i < this.decisions.length; i++) {
          this.sumDaysAndPrices(this.decisions[i].tactic);
          this.allcols[this.decisions[i].posX] = [
            ...this.allcols[this.decisions[i].posX],
            this.decisions[i],
          ];
        }
      })
      .catch((res) => this.errorServices.handleError(res));
  }

  isDecisionEmpty() {
    return this.decisions.length == 0;
  }

  isTacticsEmpty() {
    return this.tactics.length == 0;
  }

  isTacticsIdsEmpty() {
    return this.tacticIds.length == 0;
  }

  areTacticsAndTacticsIdsEmpty() {
    return this.isTacticsEmpty() && this.isTacticsIdsEmpty();
  }

  addDecision(tactic, posX) {
    this.tacticsServices
      .addDecision(this.gameId, parseInt(tactic.id.slice(1)), posX).then((res) => {
        if (res.statusText === "OK") {
          if (posX == 0) {
            this.tacticsServices.deleteDecision(res.data.id).then((deleteResponse) => {
              this.subtractDaysAndPrices(res.data.tactic);
              this.tactics.unshift(res.data.tactic);
              tactic.remove();
            })
              .catch((deleteResponse) => this.errorServices.handleError(deleteResponse));
          } else if (res.data.posX != posX) {
            this.tacticsServices.updateDecision(res.data.id, posX)
              .catch((res) => this.errorServices.handleError(res))
          }
        } else {
          this.sumDaysAndPrices(res.data.tactic);
          tactic.remove();
          this.allcols[res.data.posX] = [
            ...this.allcols[res.data.posX],
            res.data,
          ];
        }
      })
      .catch((res) => this.errorServices.handleError(res));
  }

  applyDecision(decision) {
    this.tacticsServices.applyDecision(decision.id).then(res => {
      if (res.data) {
        const feedbackOptimum = res.data.tactic.tacticScore.feedbackOptimum;
        const feedbackNonOptimum = res.data.tactic.tacticScore.feedbackNonOptimum;
        const longOptimum = feedbackOptimum.length > 50;
        const longNonOptimum = feedbackNonOptimum.length > 50;

        if (res.data.scoreLevel === "PERFECT" && res.data.tactic.tacticScore.perfect !== 0) {
          this.ngNotify.set(`Decisión aplicada oportunamente. ${feedbackOptimum}`, { type: 'success', sticky: longOptimum, position: "bottom", button: true });
        } else if (res.data.scoreLevel === "NEGATIVE") {
          this.ngNotify.set(`Decisión aplicada. ${feedbackNonOptimum}`, { type: 'error', sticky: longNonOptimum, position: "bottom", button: true });
        } else {
          this.ngNotify.set(`Decisión aplicada. ${feedbackNonOptimum}`, { type: 'info', sticky: longNonOptimum, position: "bottom", button: true });
        }
        decision.isApplied = res.data.isApplied;
        this.sumAppliedDaysAndPrices();
        const pos = this.allcols[decision.posX].indexOf(decision);
        this.allcols[decision.posX].splice(pos, 1);
        this.allcols[decision.posX].push(decision);

        this.checkGameFinished();
      }
    }).catch(res => this.errorServices.handleErrorWithPreconditions(res,
      'Su equipo no cuenta con el suficiente presupuesto o ya no cuenta con días disponibles para tomar esta decisión'));
  }

  sumDaysAndPrices(tactic) {
    this.plannedDays += tactic.days;
    this.plannedBudget += tactic.price;
  }

  sumAppliedDaysAndPrices() {
    this.gameServices.getById(this.gameId).then(res => {
      this.appliedBudget = this.gameBudget - res.data.budget;
      this.appliedDays = this.gameDays - res.data.availableDays;
    }).catch(res => this.errorServices.handleError(res));
  }

  subtractDaysAndPrices(tactic) {
    this.plannedDays -= tactic.days;
    this.plannedBudget -= tactic.price;
  }

  getAddedTacticIDs(decisions) {
    if (decisions.length != 0) {
      decisions.forEach((decision) => {
        this.tacticIds.push(decision.tactic.id);
      });
    }
  }

  getAssignItems() {
    if (!this.areTacticsAndTacticsIdsEmpty()) {
      this.tactics = this.tactics.filter(
        (tactic) => !this.tacticIds.includes(tactic.id)
      );
    }
  }

  updateGameStatus() {
    this.gameServices.updateGameStatus(this.game.id, "FINISHED_LOST").then((res) => {
      if (res.statusText === "OK") {
        this.checkGameFinished();
        this.ngNotify.set('Juego finalizado', 'success');
      }
    }).catch(error => {
      if (error.status === 409) {
        this.ngNotify.set('Lo sentimos, equipo no encontrado. Intentalo de nuevo.', 'error');
      }
    });
  }

  async getGameInformation() {
    const gameId = this.$stateParams.id;
    this.gameServices.getById(gameId).then(res => {
      this.game = res.data;
      this.appliedBudget = this.gameBudget - res.data.budget;
      this.appliedDays = this.gameDays - res.data.availableDays;
      this.deleteNotAppliedDecisions(this.game.id);
    }).catch(res => this.errorServices.handleError(res));
  }

  async deleteNotAppliedDecisions(gameId) {
    if (this.checkGameStatus()) {
      this.tacticsServices.deleteNotAppliedDecisions(gameId).then(res => {
        if (res.statusText === "OK") {
          this.$log.log("HERE 2");
        }
      }).catch(error => {
        if (error.status === 409) {
          this.ngNotify.set('Lo sentimos, equipo no encontrado. Intentalo de nuevo.', 'error');
        }
      });
    }
  }

  checkGameStatus() {
    return this.game.status === "FINISHED_LOST" || this.game.status === "FINISHED_WON";
  }

  checkGameFinished() {
    this.gameServices.getById(this.gameId).then(res => {
      const game = res.data;
      this.game = game;
      if (this.checkGameStatus()) {
        this.deleteNotAppliedDecisions();
        this.$state.go('results', { id: this.gameId });
      }
    }).catch(res => this.errorServices.handleError(res));
  }

  showConfirmationPopup() {
    return new swal({
      title: '¿Estas seguro que quieres terminar el juego?',
      showCancelButton: true,
      confirmButtonText: 'Terminar',
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.updateGameStatus();
      } else if (result.isDenied) {

      }
    })
  }
}