class ResultsController {
  /* @ngInject */
  constructor($log, $stateParams, resultServices, gameServices, errorServices, chartServices) {
    this.gameServices = gameServices;
    this.$stateParams = $stateParams;
    this.resultServices = resultServices;
    this.errorServices = errorServices;
    this.chartServices = chartServices;
    this.$log = $log;
    this.game = {};
    this.status = {};
    this.setChartOptions();
    this.actionsList = [];
    this.graphData = [];
    this.collapse = [];
    this.init();
  }

  init() {
    this.gameId = this.$stateParams.id;
    this.resultServices.getActionList(this.gameId).then(res => {
      this.actionsList = res.data;
      this.graphData = [{
        name: "Puntaje",
        children: this.chartServices.formatGraphData(res.data)
      }];
      this.gameServices.getById(this.gameId).then(res => {
        this.game = res.data;
        if (this.game.status === "FINISHED_WON") {
          this.status = '¡Felicitaciones! ¡Has logrado un cambio exitoso!';
        } else {
          this.status = '¡Sigue intentando! ¡Analiza tus resultados para ver donde puedes mejorar!';
        }
      }).catch(res => this.errorServices.handleError(res, true));
    }).catch(res => this.errorServices.handleError(res, true));
  }

  changeState(id) {
    this.chartServices.changeState(this.collapse, id);
  }

  setChartOptions() {
    this.chartOptions = this.chartServices.setGraphOptions();
  }
}

export default ResultsController;
