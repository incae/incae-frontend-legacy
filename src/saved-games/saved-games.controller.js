class SavedGamesController {
  /* @ngInject */
  constructor(gameServices, errorServices, $state, $log) {
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.$state = $state;
    this.$log = $log;
    this.selectedGame = {};
    this.loadingGames = true;
    this.gameServices.getAll().then(res => this.getSavedGames(res.data))
      .catch(res => this.errorServices.handleError(res));
  }
  getSavedGames(games) {
    this.loadingGames = false;
    this.savedGames = games;
  }
  setSelectedGame(selectedGame) {
    this.selectedGame = selectedGame;
  }
  selectGame() {
    this.$state.go('game-welcome', {id: this.selectedGame.id});
  }
}

export default SavedGamesController;
