(function (window) {
  window.__env = window.__env || {};
  // API url
  window.__env.API_URL = 'https://incae-staging-prod-v2.herokuapp.com';
  window.__env.API_CLIENT_AUTH = 'Basic aW5jYWV3ZWJjbGllbnQ6aW5jQWV3ZWJjN2llbnQ=';
}(this));