/** @ngInject */
function routesConfig($urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');
}

/* @ngInject */
function blockPrivateAccess($rootScope, $state, $log, ngNotify, sessionServices) {
  const deregistration = $rootScope.$on('$stateChangeStart', (evt, targetState) => {
    ngNotify.addTheme('customTheme', 'customNotify');
    ngNotify.config({ // eslint-disable-line angular/module-getter
      position: 'top',
      theme: 'customTheme'
    });
    if (targetState.private && !sessionServices.isLoggedIn()) {
      evt.preventDefault();
      ngNotify.set('Necesita iniciar sesión', 'info');
      sessionServices.destroy();
      if (targetState.name === 'admin' || targetState.name === 'admin-game' || targetState.name === 'admin-users' || targetState.name === 'consolidated-results') {
        $state.go('login');
      }
      if (targetState.name === 'game-welcome' || targetState.name === 'game-explanation' || targetState.name === 'main') {
        $state.go('home');
      }
    }
  });

  $rootScope.$on('$destroy', deregistration);
}

/* @ngInject */
function interceptor($httpProvider) {
  $httpProvider.interceptors.push('authorizationInjectorServices');
}

export {routesConfig, blockPrivateAccess, interceptor};
