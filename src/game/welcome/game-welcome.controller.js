class GameWelcomeController {
  /* @ngInject */
  constructor(caseServices, gameServices, ngNotify, $state, $stateParams, $log, errorServices) {
    this.gameServices = gameServices;
    this.caseServices = caseServices;
    this.errorServices = errorServices;
    this.ngNotify = ngNotify;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.currentGameId = this.$stateParams.id;
    this.currentTeam = {};
    this.companyName = {};
    this.loading = true;
    this.newName = "";
    this.regex = /^[a-zA-Z_\s]*$/;
    this.init();
  }

  init() {
    this.gameServices.getById(this.currentGameId).then(res => {
      this.currentTeam = res.data.name;
      this.caseServices.getCase().then(res => {
        this.companyName = res.data[0].name;
        this.loading = false;
      }).catch(res => {
        this.errorServices.handleError(res, true);
      });
    }).catch(res => {
      this.errorServices.handleError(res, true);
    });
  }

  goToExplanation() {
    this.$state.go('game-explanation', {id: this.currentGameId});
  }

  updateGameName() {
    if (!this.newName) {
      this.ngNotify.set('El nombre de equipo es requerido', 'error');
    } else if (this.regex.test(this.newName)) {
      this.gameServices.updateGameName(this.currentGameId, this.newName).then(() => {
        angular.element('#rename-game').modal('hide');
        if (angular.element('.modal-backdrop').is(':visible')) {
          angular.element('body').removeClass('modal-open');
          angular.element('.modal-backdrop').remove();
        }
        this.$state.reload();
        this.newName = "";
      }).catch(error => {
        if (error.status === 409) {
          this.ngNotify.set('Lo sentimos, este nombre ya existe. Por favor elige otro.', 'error');
        }
      });
    } else {
      this.ngNotify.set('El nombre del equipo no debe contener caracteres especiales o números', 'error');
    }
  }
}

export default GameWelcomeController;
