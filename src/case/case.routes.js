/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('cases', {
      url: '/game/:id/cases',
      template: require('./case.html'),
      controller: 'CaseController as vm'
    });
}

export default routes;
