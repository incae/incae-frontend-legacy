import GameCreateController from './game-create.controller';
import './game-create.scss';

const gameCreate = {
  template: require('./game-create.html'),
  controller: GameCreateController,
  controllerAs: 'vm'
};

export default
  angular
    .module('gameCreate', [])
    .component('gameCreate', gameCreate)
    .name;
