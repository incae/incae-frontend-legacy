import {CaseServices} from './case.services';
import CaseController from './case.controller';
import ngSanitize from 'angular-sanitize';
import routes from './case.routes';
import './case.scss';

export default
  angular
    .module('incae.cases', [ngSanitize])
    .config(routes)
    .controller('CaseController', CaseController)
    .service('caseServices', CaseServices)
    .name;
