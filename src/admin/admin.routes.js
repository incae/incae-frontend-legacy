/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('admin', {
      url: '/admin',
      template: require('./admin.html'),
      controller: 'AdminController as vm',
      private: true
    })
    .state('admin-game', {
      url: '/admin/game/:id',
      template: require('./game/admin-game.html'),
      controller: 'AdminGameController as vm',
      private: true
    })
    .state('admin-users', {
      url: '/admin/users',
      template: require('./users/admin-users.html'),
      controller: 'AdminUsersController as vm',
      private: true
    })
    .state('login', {
      url: '/login',
      template: require('./login/login.html'),
      controller: 'LoginController as vm'
    })
    .state('consolidated-results', {
      url: '/consolidated-results',
      template: require('./consolidated-results/consolidated-results.html'),
      controller: 'ConsolidatedResultsController as vm',
      private: true
    });
}

export default routes;
