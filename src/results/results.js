import routes from './results.routes';
import resultsController from './results.controller';
import {ResultServices} from './results.services';
import {GameServices} from '../game/game.services';
import {ChartServices} from '../chart/chart.services';
import './results.scss';

export default
  angular
    .module('incae.results', [])
    .config(routes)
    .controller('ResultsController', resultsController)
    .service('resultServices', ResultServices)
    .service('gameServices', GameServices)
    .service('chartServices', ChartServices)
    .name;
