class ConsolidatedResultsController {
  /* @ngInject */
  constructor(errorServices, $stateParams, $log, gameServices) {
    this.errorServices = errorServices;
    this.gameServices = gameServices;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.allActionsList = [];
    this.graphData = [];
    this.collapse = [];
    this.setGraphOptions();
    this.init();
  }

  init() {
    this.gameServices.getAllDecision().then(res => {
      this.allActionsList = res.data;
      this.graphData = [{
        name: "Puntaje",
        children: this.formatGraphData(res.data)
      }];
    }).catch(res => this.errorServices.handleError(res));
  }

  changeState(id) {
    this.collapse[id] = !this.collapse[id];
  }

  setGraphOptions() {
    this.options = {
      chart: {
        type: "sunburstChart",
        height: 390,
        duration: 250,
        dispatch: {},
        sunburst: {
          dispatch: {},
          width: 350,
          height: 450,
          mode: "size",
          id: 2547,
          duration: 500,
          groupColorByParent: true,
          showLabels: false,
          labelThreshold: 0.02,
          margin: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
          }
        },
        tooltip: {
          valueFormatter: (d => {
            return d ? parseFloat(d).toFixed(2) : 0;
          }),
          duration: 0,
          gravity: "w",
          distance: 15,
          snapDistance: 0,
          classes: null,
          chartContainer: null,
          enabled: true,
          hideDelay: 200,
          headerEnabled: false,
          fixedTop: null,
          offset: {
            left: 0,
            to: 0
          },
          hidden: true,
          data: null,
          id: "nvtooltip-74967"
        },
        width: 450,
        mode: "size",
        groupColorByParent: true,
        showLabels: false,
        labelThreshold: 0.02,
        margin: {
          top: 30,
          right: 20,
          bottom: 20,
          left: 20
        },
        noData: null,
        defaultState: null
      },
      styles: {
        classes: {
          "with-3d-shadow": true,
          "with-transitions": true,
          "gallery": false
        },
        css: {}
      }
    };
  }

  stageChartColor(stageName) {
    switch (stageName) {
      case 'Documentar':
        return '#E18434';
      case 'Identificar las causas':
        return '#943139';
      case 'Mapear':
        return '#409090';
      case 'Definir Vision':
        return '#3F97F8';
      case 'Comunicar':
        return '#41D532';
      case 'Implementar y Medir':
        return '#A632D5';
      default:
        return '#ED857C';
    }
  }

  formatGraphData(rawData) {
    const result = [];
    rawData.forEach(stage => {
      const formattedStage = {
        name: stage.name,
        color: this.stageChartColor(stage.name),
        children: []
      };

      stage.decisions.forEach(decision => {
        const formattedDecision = {
          name: decision.name,
          size: decision.gainScore,
          color: this.stageChartColor(stage.name)
        };

        formattedStage.children.push(formattedDecision);
      });

      result.push(formattedStage);
    });

    return result;
  }
}

export default ConsolidatedResultsController;
