import angular from 'angular';
import 'angular-ui-router';
import 'angular-ui-router/release/stateEvents';
import 'ngstorage';
import 'ng-notify';
import 'angularjs-slider';
import 'angularjs-slider/dist/rzslider.css';
import 'angular-nvd3';
import modal from 'angular-ui-bootstrap/src/modal';
import 'ng-google-charts';

import {ErrorServices} from './error.services';
import {SessionServices} from './session.services';
import {AuthorizationInjectorServices} from './authorization-injector.services';
import navbar from './navbar/navbar';
import bottomNavbar from './bottom-navbar/bottom-navbar';

export default
  angular
    .module('incae.common', [
      'ui.router',
      'ui.router.state.events',
      'ngStorage',
      'ngNotify',
      'rzModule',
      navbar,
      bottomNavbar,
      'nvd3',
      modal,
      'ngGoogleCharts'
    ])
    .service('errorServices', ErrorServices)
    .service('sessionServices', SessionServices)
    .service('authorizationInjectorServices', AuthorizationInjectorServices)
    .name;
