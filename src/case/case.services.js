export class CaseServices {
  /* @ngInject */
  constructor(API_URL, $log, $http, $q) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
    this.$q = $q;
  }

  getCase() {
    return this.$http.get(`${this.API_URL}/cases`);
  }
}
