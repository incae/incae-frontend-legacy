export class UserServices {
  /* @ngInject */
  constructor(API_URL, $log, $http) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
  }

  getAll() {
    return this.$http.get(`${this.API_URL}/users`);
  }

  add(user) {
    return this.$http.post(`${this.API_URL}/users`, angular.toJson(user));
  }

  delete(id) {
    return this.$http.delete(`${this.API_URL}/users/${id}`);
  }

  getAllUsersByRole(roleName) {
    return this.$http.get(`${this.API_URL}/users-by-role/${roleName}`);
  }

  userRoleValidation(login, roles) {
    return this.$http.post(`${this.API_URL}/user-validation/${login}`, angular.toJson(roles));
  }
}
