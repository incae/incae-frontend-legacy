export class NavbarServices {
  /* @ngInject */
  constructor($log) {
    this.$log = $log;
    this.collapseState = false;
  }

  setCollapseState(state) {
    this.collapseState = state;
  }

  getCollapseState() {
    return this.collapseState;
  }

  toggleCollapseState() {
    this.collapseState = !this.collapseState;
  }
}
