export class GameServices {
  /* @ngInject */
  constructor(API_URL, $log, $http) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
    this.games = '/games';
    this.teamMembers = '/teamMembers';
    this.teamMemberById = '/teamMembers?gameId=';
    this.decisionsByStage = '/all-decisions-by-stage';
    this.results = '/games-results';
    this.gameByUsername = '/game-by-username';
    this.gameNewName = '/game?newName=';
  }

  addGame(game) {
    return this.$http.post(`${this.API_URL}${this.games}`, angular.toJson(game));
  }

  delete(id) {
    return this.$http.delete(`${this.API_URL}${this.games}/${id}`);
  }

  addTeamMembers(teamMembers, gameId) {
    return this.$http.post(`${this.API_URL}${this.teamMembers}?newGameId=${gameId}`, angular.toJson(teamMembers));
  }

  getTeamMembers(id) {
    return this.$http.get(`${this.API_URL}${this.teamMemberById}${id}`);
  }

  getAll() {
    return this.$http.get(`${this.API_URL}${this.games}`);
  }

  getById(id) {
    return this.$http.get(`${this.API_URL}${this.games}/${id}`);
  }

  getAllDecision() {
    return this.$http.get(`${this.API_URL}${this.decisionsByStage}`);
  }

  getReport() {
    return this.$http.get(`${this.API_URL}${this.results}`);
  }

  getGameByUsername(username) {
    return this.$http.post(`${this.API_URL}${this.gameByUsername}/${username}`);
  }

  updateGameName(id, newGameName) {
    return this.$http.patch(`${this.API_URL}${this.games}/${id}`, newGameName);
  }

  updateGameStatus(id, status) {
    return this.$http.patch(`${this.API_URL}${this.games}/updateStatus/${id}`, status);
  }

  updateDaysAndPriceGame(id, tacticDays, tacticPrice) {
    const tactic = {
      days: tacticDays,
      price: tacticPrice
    };
    return this.$http.put(`${this.API_URL}${this.games}/${id}`, angular.toJson(tactic));
  }
}
