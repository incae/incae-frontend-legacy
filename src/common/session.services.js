export class SessionServices {
  /* @ngInject */
  constructor($log, $window) {
    this.$log = $log;
    this.$window = $window;
    this.currentUser = {};
  }

  destroy() {
    this.currentUser = {};
    this.$window.localStorage.removeItem('user-session');
  }

  getCurrentUser() {
    return this.getAuthData().user;
  }

  isLoggedIn() {
    return this.getAuthData() && this.getAuthData().user && this.getAuthData().user.isLoggedIn;
  }

  getAuthData() {
    const session = this.$window.localStorage.getItem('user-session');
    if (session) {
      return angular.fromJson(session);
    }
  }

  setAuthData(user, token) {
    this.currentUser = user || {};
    this.currentUser.isLoggedIn = true;
    const session = {
      user: this.currentUser,
      token
    };
    this.$window.localStorage.setItem('user-session', angular.toJson(session));
  }

  getAuthorization() {
    const token = this.getAuthData() && this.getAuthData().token ? this.getAuthData().token.access_token : '';
    return `Bearer ${token}`;
  }
}
