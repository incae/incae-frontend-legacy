class AdminController {
  /* @ngInject*/
  constructor(gameServices, errorServices, $state, $stateParams, $log) {
    this.gameServices = gameServices;
    this.errorServices = errorServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.games = [];
    this.loadingGames = true;
    this.sortType = 'name';
    this.reverse = true;
    this.init();
  }

  init() {
    this.gameServices.getAll()
      .then(res => {
        this.loadingGames = false;
        this.games = res.data;
      })
      .catch(res => this.errorServices.handleError(res));
  }

  viewGame(game) {
    this.$state.go('admin-game', {id: game.id});
  }

  deleteGame(game) {
    this.gameServices.delete(game.id).then(() => {
      this.$state.reload();
    }).catch(res => this.errorServices.handleError(res));
  }

  sortTable(sortType) {
    this.reverse = (this.sortType === sortType) ? !this.reverse : false;
    this.sortType = sortType;
  }

  generateCSVreport() {
    this.gameServices.getReport()
      .then(res => {
        const report = angular.element('a');
        report.attr({
          href: 'data:attachment/csv;charset=utf-8,'.concat(encodeURI(res.data)),
          target: '_blank',
          download: 'Resultados.csv'
        })[0].click();
      }).catch(error => this.errorServices.handleError(error));
  }

  updateTeamsList() {
    angular.element('#create-team-modal').modal('hide');
    this.$state.reload();
  }
}

export default AdminController;
