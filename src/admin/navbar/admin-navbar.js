import AdminNavbarController from './admin-navbar.controller';
import './admin-navbar.scss';

const adminNavbar = {
  template: require('./admin-navbar.html'),
  controller: AdminNavbarController,
  controllerAs: 'vm'
};

export default
  angular
    .module('adminNavbar', [])
    .component('adminNavbar', adminNavbar)
    .name;
