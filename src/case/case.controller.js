class CaseController {
  /* @ngInject*/
  constructor(caseServices, errorServices, $state, $stateParams, $log) {
    this.caseServices = caseServices;
    this.errorServices = errorServices;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$log = $log;
    this.cases = {};
    this.tabs = [];
    this.gameId = this.$stateParams.id;
    this.getCase();
  }

  getCase() {
    this.caseServices.getCase().then(res => {
      this.cases = res.data;
      this.cases[0].casesInfo.forEach(information => {
        this.tabs.push({title: information.title, content: information.content});
      });
      this.selected = this.tabs[0];
    }).catch(res => {
      this.errorServices.handleError(res, true);
    });
  }

  select(tab) {
    angular.element("#scroll").scrollTop(0);
    this.selected = tab;
  }

  goToGame() {
    this.$state.go('main', {id: this.gameId});
  }

}

export default CaseController;
