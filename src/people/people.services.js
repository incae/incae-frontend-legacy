export class PeopleServices {
  /* @ngInject */
  constructor(API_URL, $log, $http) {
    this.API_URL = API_URL;
    this.$log = $log;
    this.$http = $http;
  }

  getAllByDepartmentId(id) {
    return this.$http.get(`${this.API_URL}/employees?departmentId=${id}`);
  }

  getAllDepartmentsByGameId(gameId) {
    return this.$http.get(`${this.API_URL}/departments?gameId=${gameId}`);
  }

  getPerson(idPerson) {
    return this.$http.get(`${this.API_URL}/employees/${idPerson}`);
  }

  getAllInfo() {
    return this.$http.get(`${this.API_URL}/employees_info`);
  }
}
