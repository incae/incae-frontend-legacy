/* @ngInject */
function routes($stateProvider) {
  $stateProvider
    .state('main', {
      url: '/game/:id',
      template: require('./main.html'),
      controller: 'GameController as vm',
      params: {department: null},
      private: true
    })
    .state('game-welcome', {
      url: '/game/:id/welcome',
      template: require('./welcome/game-welcome.html'),
      controller: 'GameWelcomeController as vm',
      private: true
    })
    .state('game-explanation', {
      url: '/game/:id/explanation',
      template: require('./explanation/game-explanation.html'),
      controller: 'GameExplanationController as vm',
      private: true
    });
}

export default routes;
