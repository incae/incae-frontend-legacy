import homeController from './home.controller';
import routes from './home.routes';
import './home.scss';

export default
  angular
    .module('incae.home', [])
    .config(routes)
    .controller('HomeController', homeController)
    .name;
